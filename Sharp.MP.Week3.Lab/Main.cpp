//Lab Cards?
//Week 3
#include <iostream>
#include <conio.h>

using namespace std;

enum Rank {Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};

enum Suit { Hearts, Spades, Diamonds, Clubs};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	if (card.Rank == Ten) cout << "ten";
	else cout << "five";
	cout << " of ";
	if (card.Suit == Spades) cout << "spades" << "\n";
	else cout << "diamonds" << "\n";

};

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank) return card1;
	return card2;
}

int main() 
{
	Card card1;
	card1.Rank = Ten;
	card1.Suit = Spades;

	Card card2;
	card2.Rank = Five;
	card2.Suit = Diamonds;
	string card;

	HighCard(card1, card2);

	PrintCard(card1);
	PrintCard(card2);


	_getch();
	return 0;
}